from collections import namedtuple 

twice = namedtuple("twice","x y")
k = twice(3,4)
g = twice(2,5)

arr = [k,g]
arr.sort(key=lambda m:m.y)
print(arr)