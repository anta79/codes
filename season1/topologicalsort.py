class node:
	
	def __init__(self,num):
		self.num = num 
		self.adj = []
		self.start = self.end = -1
	def add(self,n):
		self.adj.append(n) 

	def __str__(self):
		return "num {} start {} end {}".format(self.num,self.start,self.end)
time = 0
def dfs1(g):
	for n in g :
		if(n.start == -1) :
			dfs2(n)

def dfs2(n):
	global time
	time += 1 
	n.start = time 
	for N in n.adj :
		if(N.start == -1) :
			dfs2(N)
	time += 1 
	n.end = time 

a = node(1)
b = node(2)
c = node(3)
d = node(4)
e = node(5)
f = node(6)

a.add(b)
a.add(c) 
c.add(d)
c.add(e) 
e.add(f)

graph = [a,b,c,d,e,f]

dfs1(graph)

for ele in graph :
	print(ele)