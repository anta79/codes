import sys,math

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

def if2pow(n):
	for i in range(1,33):
		if(2**i == n):
			return True 
		if(2**i > n):
			return False 
	return False

def isPrim(n):
	p = int(math.sqrt(n))+1 
	ans = True 
	for i in range(2,p+1):
		if(n % i == 0):
			ans = False 
			break
	return ans 

isPrime = [True]*100001

isPrime[0] = isPrime[1] = False 

b = int(math.sqrt(10001)+2)

for i in range(2,b) :
	if isPrime[i] :
		for j in range(i*i,100001,i):
			isPrime[j] = False 

tc = int(input()) 

for zzz in range(tc):
	n = int(input()) 
	ans = ""

	if(n == 1):
		ans = "FastestFinger"
	elif(n % 2 == 1 or n == 2) :
		ans = "Ashishgup"
	else :
		if(if2pow(n)):
			ans = "FastestFinger"
		else:
			n //= 2 
			if(isPrim(n)) :
				ans = "FastestFinger"
			else:
				ans = "Ashishgup"
	print(ans)
