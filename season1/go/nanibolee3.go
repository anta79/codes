package main 

import "fmt" 

func gcd(a,b int64) int64 {
	if a == 0 {
		return b 
	}
	return gcd(b%a,a)
	
}
func main() {
	fmt.Print(gcd(6,4))
}