package main

import (
	"fmt"
	"strconv"
)

func main() {
	var pi float64 = 67.98
	fmt.Print(strconv.Itoa(int(pi)))
}
