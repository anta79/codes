from bisect import bisect_left 

ans = ""
t = 1 
while (True):

	n,q = map(int,input().split())
	if(n == 0 and q == 0) :
		break 
	ans += "CASE# {}:\n".format(t)
	arr = [0]*n 
	for i in range(n) :
		arr[i] = int(input()) 

	arr.sort() 
	for i in range(q) :
		query = int(input())
		f = bisect_left(arr,query) 
		if(f < n and arr[f] == query) :
			ans += "{} found at {}\n".format(query,f+1)
		else :
			ans += "{} not found\n".format(query)
	t += 1
print(ans,end="")
