#include <stdio.h> 

int min(int a,int b) {
	if(a < b) {
		return a ;
	}
	else {
		return b ;
	}
}

void floydWarshall(int V,int arr[V][V]) {
	int k,i,j ;
	for(k=0;k<V;k++) {
		for(i=0;i<V;i++) {
			for(j=0;j<V;j++) {
				arr[i][j] = min(arr[i][j],arr[i][k]+arr[k][j]);
			}
		}
	}
}


int main() {

	int n,k,q,sum ;
	int i,j,a,b,c ; 

	scanf("%d %d",&n,&k) ;
	int arr[n][n] ;

	for(i=0;i<n;i++) {
		for(j=0;j<n;j++) {
			arr[i][j] = 200000000 ;
		}
	}

	for(i=0;i<k;i++) {
		scanf("%d %d %d",&a,&b,&c) ; 
		if(arr[a][b] > c) {
			arr[a][b] = c ;
			arr[b][a] = c ;
		}
	}
	for(i=0;i<n;i++) {
		arr[i][i] = 0 ;
	}

	floydWarshall(n,arr);
	sum = 0 ;
	scanf("%d",&q) ;
	for(i=0;i<q;i++){
		scanf("%d %d",&a,&b) ;
		sum += arr[a][b] ;
	}
	printf("%d\n",sum);
}