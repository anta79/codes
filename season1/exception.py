def boxPrint(symbol,w,h):

	if(len(symbol)) != 1 :
		raise Exception('symbol must be a single character string') 
	if(w <= 2):
		raise Exception("Width must be greater than 2") 
	if(h <= 2):
		raise Exception("Height must be greater than 2") 

	print(symbol*w) 
	for i in range(h-2):
		print(symbol + (' '*(w-2))+symbol)
	print(symbol*w)


for sym,w,h in (('*',3,3),('O',5,5),('x',3,2)):
	try :
		boxPrint(sym,w,h)
	except Exception as err :
		print("An exception happened: " + str(err))