from math import ceil
tc = int(input())
# print(tc)
for zzz in range(tc):
	h,c,t = map(int,input().split()) 
	# print(h,c,t)
	m = ( h+c ) / 2 
	if(abs(m-t) == 0) :
		print(2)
	else :
		i =1 
		dif = 10**18
		bal = 100
		while(bal > 0) :
			bal -= 1
			# print(i)
			v = ceil(i/2)
			w = i - v 

			temp = (v*h + w*c) / i 

			temp2 = abs(temp-t)
			# print(temp2)
			if(dif < temp2) :
				# print("YES")
				break 
			else :
				dif = temp2

			i += 2

		if(dif < abs(m-t)) :
			print(i-2)
		else :
			print(2)
