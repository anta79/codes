def closestindex(arr,num,start):
    a = start
    b = len(arr) - 1
    while(a < b) :
        mid = (a + b) // 2
        if(num < arr[mid]) :
            if (abs(arr[mid] - num) <= abs(arr[mid-1] - num)) :
                return mid
            else :
                b = mid - 1
        else  :
            if (abs(arr[mid] - num) < abs(arr[mid+1] - num) ):
                return mid
            else :
                a = mid + 1

    return  a

print(closestindex([80,90],85,0))