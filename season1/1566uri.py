from collections import Counter
tc = int(input()) 
ans = ""
for ll in range(tc):
	n = int(input()) 
	arr = [0]*231
	a = input().split() 
	for ele in a :
		arr[int(ele)] += 1
	temp = ""
	for i in range(231):
		temp += "{} ".format(i)*arr[i]

	ans += temp[0:-1]
	ans += "\n"
print(ans,end="")
