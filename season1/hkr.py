from collections import deque,defaultdict
from heapq import *
dq = deque()
dic = defaultdict(int)
heap = [] 
t = int(input())
maxi = 0
for i in range(t): 
	q = input() 
	if(q == "2") :
		k = dq.pop()
		dic[k] -= 1
		if(dic[k] == 0 and k == maxi) :
			while(heap):
				maxi = -1*heappop(heap)
				if(dic[maxi] > 0):
					heappush(heap,-maxi)
					break 
				maxi = 0
	elif(q == "3") :
		print(maxi)
	else :
		k = int(q[2:])
		if(dic[k] == 0) :
			heappush(heap,-k)
		dq.append(k)
		dic[k] += 1
		maxi = max(maxi,k)