tc = gets.to_i 
tc.times do 
	n = gets.to_i 
	arr = gets.split().map {|i| i.to_i} 
	brr = Array.new(n) {0}

	for i in 0...n 
		if(i+1 == arr[i]) 
			brr[i] = 1 
		end
	end
	f = l = -1 
	for i in 0...n 
		if(brr[i] == 0) 
			f = i 
			break
		end
	end
	for i in (n..0).step(-1) 
		if(brr[i] == 0) 
			l = i 
			break
		end
	end
	ans = 0 
	if(f != -1) 
		ans += 1 
		for i in f..l 
			if(brr[i] == 1) 
				ans += 1
				break 
			end
		end
	end
	puts(ans)
end