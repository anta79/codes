import sys
def reading(arg):
    if(arg == ""):
        file = open('input.txt')
        return file.read().splitlines()
    else :
        return sys.stdin.read().splitlines()
input = iter(reading("")).__next__

print(input())
