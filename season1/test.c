#include <stdio.h>

long long test(long long m) {
	long long p = 1 ;
	long long j ;
	for(j=1;;j++) {

		if(p >= m) {
			break ;
		}
		p *= 2;
		
		// printf("%d\n",j);
	}
	if(p == m) {
		return j ;
	}
	else {
		return -1 ;
	}
}


int main() {
	long long tc ,i,g,ans,a,b,temp,res;

	scanf("%lld",&tc);
	for(i=0;i<tc;i++) {
		scanf("%lld %lld",&a,&b); 
		if(a > b) {
			temp = a ;
			a = b;
			b = temp ;
		}
		if(b % a == 0) {
			
			res = b / a ;
			if(test(res) == -1) {
				puts("-1");
			}
			else {
				ans = 0;
				g = res ;
				while(g != 1) {
					if(g >= 8) {
						ans += 1 ;
						g /= 8;
					}
					else if(g >= 4) {
						ans += 1;
						g /=  4;
					}
					else if(g >= 2) {
						ans += 1;
						g /= 2 ;
					}
				}
				printf("%lld\n",ans);
			}
		}
		else {
			puts("-1");
		}
	}
}