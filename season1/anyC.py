import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

tc = int(input()) 

for zzz in range(tc):
	n,k = map(int,input().split())
	arr = list(map(int,input().split())) 
	v = list(map(int,input().split()))
	w =[]
	for i in range(k): 
		w.append((i,v[i]))

	arr.sort(key=lambda x: -x) 
	w.sort(key=lambda x: x[1]) 
	cur = [0]*k
	maxi = [0]* k 
	mini = [0]* k 
	j = 0

	for i in range(k) :
		maxi[w[i][0]] = arr[j]
		cur[i] += 1 
		j += 1 


	j -= 1
	for ele in w :
		pos = ele[0] 
		needed = ele[1] - cur[pos]
		j += needed
		if(needed == 0):
			mini[pos] = maxi[pos]
		else :
			mini[pos] = arr[j] 
	# print(mini)
	# print(maxi) 
	sumi = 0 
	for i in range(k):
		sumi += mini[i] + maxi[i] 
	print(sumi)

