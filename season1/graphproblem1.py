from itertools import count

def dfs1(graph,visited,cparent) :
	ans = 0 
	l = len(graph) 
	for i in range(l) :
		if(not visited[i]) :
			ans += 1
			cparent[i] = i 
			dfs2(graph,visited,i,cparent)
	return ans 

def dfs2(graph,visited,s,cparent):

	visited[s] = True 
	for node in graph[s] :
		if (not visited[node]):
			cparent[node] = cparent[s]
			dfs2(graph,visited,node,cparent)


db = {}
c = count()

n, k= map(int,input().split())

graph = [[] for i in range(n)]
visited = [False]*n 
cparent = [0]*n

for l in range(k) :
	nm1,_,nm2 = input().split() 
	if nm1 not in db  :
		db[nm1] = next(c) 
	if nm2 not in db :
		db[nm2] = next(c) 
	graph[db[nm1]].append(db[nm2])
	graph[db[nm2]].append(db[nm1])

# print(graph)
# print(visited)
print(dfs1(graph,visited,cparent))
print(cparent)
