tc = int(input()) 
for l in range(tc):
	marked = [[] for i in range(50)]
	for i in range(50) :
		marked[i] = [False]*50
		
	_a = int(input()) 
	_b,n = map(int,input().split()) 

	ans = 0
	for i in range(n): 
		c,d = map(int,input().split()) 
		
		if(not marked[c][d]) :
			marked[c][d] = True 
			marked[d][c] = True
			ans += 1 
	print(ans*2)