import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

tc = int(input()) 

for zz in range(tc):
    s = input() 
    l = len(s)
    pl = 0 
    mi = 0 

    parr = [0]*l 
    marr = [0]*l 
    for i in range(l):
        if(s[i] == '+') :
            pl += 1 
        else :
            mi += 1 
        parr[i] = pl 
        marr[i] = mi 
    # print(parr,marr)

    ans  = 0
    seq = 0
    for i in range(l): 
        if(marr[i] > seq+parr[i]) :
            ans += (i+1) 
            seq += 1  
    print(ans+l)