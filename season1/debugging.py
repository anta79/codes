from logging import *
basicConfig(level=DEBUG)
disable(DEBUG)

def factorial(n):
    debug('Start of factorial {}'.format(n))
    total = 1
    for i in range(1,n + 1):
        total *= i
        debug("i = {} , total = {}".format(i,total))
    debug('End of factorial {}'.format(n))
    return total

print(factorial(5))
