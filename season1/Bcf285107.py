import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

n = int(input()) 

sumi = 0 

for i in range(1,n+1):
	sumi += 1/i 
print(sumi)

