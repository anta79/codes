import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

tc = int(input()) 

for zz in range(tc):
	n,x,m = map(int,input().split())

	l,h = x,x 
	for i in range(m):
		a,b = map(int,input().split())  
		if(a > b):
			a,b = b,a 
		if(a <= l and b >= h) :
			l = a 
			h = b 
		elif(a < l and b >= l) :
			l = a 
		elif(a <= h and b > h) :
			h = b
	print(h-l+1)