def countLargestGroup(n):
	arr = [0] * 37 
	for i in range(0,n+1) :
		t = i 
		sum = 0
		while(t > 0 ):
			sum += t % 10 
			t //= 10 
		arr[sum] += 1 
	return arr

print(countLargestGroup(2))