def dfs1(graph,visited,cparent) :
	ans = 0 
	l = len(graph) 
	for i in range(l) :
		if(not visited[i]) :
			ans += 1
			cparent[i] = i 
			dfs2(graph,visited,i,cparent)
	return ans 

def dfs2(graph,visited,s,cparent):

	visited[s] = True 
	for node in graph[s] :
		if (not visited[node]):
			cparent[node] = cparent[s]
			dfs2(graph,visited,node,cparent)


n,p,q = map(int,input().split()) 
graph = [[] for i in range(n)] 
visited = [False]*n 
cparent = [0]*n
ans = ""

for i in range(p):
	u,v = map(int,input().split()) 
	graph[u-1].append(v-1)
	graph[v-1].append(u-1) 

_ = dfs1(graph,visited,cparent)
for i in range(q):
	u,v = map(int,input().split()) 
	if(cparent[u-1] == cparent[v-1]) :
		ans += "Lets que lets\n" 
	else :
		ans += "Deu ruim\n"

print(ans,end="")