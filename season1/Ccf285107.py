from math import ceil
h,c,t = map(int,input().split()) 

for i in range(1,1000):
	m = ceil(i/2) 
	n = i- m  
	p = ((m*h)+(n*c)) / i
	print(abs(p-t))