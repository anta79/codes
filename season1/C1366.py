import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

tc = int(input()) 

for zz in range(tc):
	sar,col = map(int,input().split()) 

	arr = [[0]*col for i in range(sar)]


	cont = [[] for i in range(sar+col)]

	for i in range(sar):
		temp = list(map(int,input().split())) 
		for j in range(col) :
			arr[i][j] = temp[j] 

	for i in range(sar):
		for j in range(col): 
			fol = i+j+1 
			cont[fol].append(arr[i][j]) 
	# print(cont) 

	repe = sar+col-1 
	bare = (repe // 2) + 1

	ans = 0 
	for i in range(1,bare) :
		ele1 = cont[i] 
		ele2 = cont[repe-i+1]
		ones = ele1.count(1)+ele2.count(1) 
		zers = ele1.count(0)+ele2.count(0) 

		ans += min(ones,zers)

	print(ans)

