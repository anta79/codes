from sys import stdin
def closestindex(arr,num,start):
    a = start
    b = len(arr) - 1
    while(a < b) :
        mid = (a + b) // 2
        if(num < arr[mid]) :
            if (abs(arr[mid] - num) < abs(arr[mid-1] - num)) :
                return mid
            else :
                b = mid - 1
        else  :
            if (abs(arr[mid] - num) < abs(arr[mid+1] - num) ):
                return mid
            else :
                a = mid + 1

    return  a

output = ""

tc = int(stdin.readline())
for l in range(tc):
    l1,l2,l3 = map(int,stdin.readline().strip().split())
    c =  [int(x) for x in stdin.readline().strip().split()]
    b =  [int(x) for x in stdin.readline().strip().split()]
    a =  [int(x) for x in stdin.readline().strip().split()]

    a.sort()
    b.sort()
    c.sort()
    
    fun = closestindex

    k = 0
    g = 0
    ans = (10**18) * 3

    for i in range(len(c)):
        k = fun(a,c[i],k)
        # print("k {}".format(k))
        t1 = ((a[k]-c[i]) * (a[k]-c[i]))
        g = fun(b,c[i],g)
        # print("g {}".format(g))
        t2 = ((b[g]-c[i]) * (b[g]-c[i]))
        t3 = ((a[k] - b[g]) * (a[k] - b[g]))
        # print(t1,t2,t3)
        ans = min(t1+t2+t3,ans)

    k = g =0
    for i in range(len(b)):
        k = fun(a,b[i],k)
        # print("k {}".format(k))
        t1 = ((a[k]-b[i]) * (a[k]-b[i]))
        g = fun(c,b[i],g)
        # print("g {}".format(g))
        t2 = ((c[g]-b[i]) * (c[g]-b[i]))
        t3 = ((a[k] - c[g]) * (a[k] - c[g]))
        # print(t1,t2,t3)
        ans = min(t1+t2+t3,ans)
    k = g = 0

    for i in range(len(a)):
        k = fun(b,a[i],k)
        # print("k {}".format(k))
        t1 = ((b[k]-a[i]) * (b[k]-a[i]))
        g = fun(c,a[i],g)
        # print("g {}".format(g))
        t2 = ((c[g]-a[i]) * (c[g]-a[i]))
        t3 = ((b[k] - c[g]) * (b[k] - c[g]))
        # print(t1,t2,t3)
        ans = min(t1+t2+t3,ans)
    output += "{} \n".format(ans)
print(output)
