import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

tc = int(input()) 

for zz in range(tc):
	a,b = map(int,input().split())

	if(b < a):
		a,b = b,a 

	m = b - a 
	if(m >= a):
		print(a)
	else :
		a -= m 
		n = (2*a) // 3
		print(m+n)

	# ans = 0
	# while (True):
	# 	if(a < 1 or b < 2):
	# 		break 
	# 	b -= 2 
	# 	a -= 1 
	# 	ans += 1
	# 	if(b < a):
	# 		a,b = b,a
	# print(ans)
