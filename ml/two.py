# %%
import matplotlib.pyplot as plt
import  numpy as np

x = np.linspace(-10,10,100)
y = np.sin(x)
plt.plot(x,y,marker="x")

# %%
from sklearn.datasets import load_iris
iris_dataset = load_iris()
print(iris_dataset["feature_names"])

# %%
from sklearn.model_selection import train_test_split 
Xtrain,Xtest,ytrain,ytest = train_test_split(
iris_dataset['data'],iris_dataset['target'],random_state=0
)

# print("X_train shape: {}".format(Xtrain.shape))
# print("y_tran shape: {}".format(ytrain.shape))
print(Xtrain)
