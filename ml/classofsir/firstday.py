# %%
weather =['Sunny','Sunny','Overcast','Rainy','Rainy','Rainy','Overcast','Sunny','Sunny','Rainy','Sunny','Overcast','Overcast','Rainy']
temp =['Hot','Hot','Hot','Mild','Cool','Cool','Cool','Mild','Cool','Mild','Mild','Mild','Hot','Mild']
play =['No','No','Yes','Yes','Yes','No','Yes','No','Yes','Yes','Yes','Yes','Yes','No']

# Import LabelEncoder which encodes labels with a value between 0 and n_classes-1
from sklearn import preprocessing
#creating labelEncoder
le = preprocessing.LabelEncoder()
# print(le)
# Converting string labels into numbers.
weather_encoded=le.fit_transform(weather)
temp_encoded=le.fit_transform(temp)
label=le.fit_transform(play)
print("Temp:",temp_encoded)
print("Play:",label)
print("Weather:", weather_encoded)
features = zip(weather_encoded,temp_encoded)
# %%

from sklearn.naive_bayes import GaussianNB
#Create a Gaussian Classifier
model = GaussianNB()
features = list(features)
# Train the model using the training sets
model.fit(features,label)
predicted = model.predict([[0,2]]) # 0:Overcast, 2:Mild print("Predicted Value:", predicted)
print("Predicted Value:", predicted)
