# %%
from sklearn import datasets
#Load dataset
wine = datasets.load_wine()
print("Features: ", wine.feature_names)
# print the label type of wine(class_0, class_1, class_2)
print("Labels: ", wine.target_names)
wine.data.shape
# print the wine data features (top 5 records)
# %%
print(wine.data[0:5])
# print the wine labels (0:Class_0, 1:class_2, 2:class_2)
# %%
print(wine.target)
# %%
# Import train_test_split function
from sklearn.model_selection import train_test_split
# Split dataset into training set and test set
X_train, X_test, y_train, y_test = train_test_split(wine.data, wine.target,
                                    test_size=0.3,random_state=2) # 70% training and 30% test
# %%
from sklearn.naive_bayes import GaussianNB
model = GaussianNB()
model.fit(X_train,y_train)
y_pred = model.predict(X_test)
# %%
from sklearn import metrics
# Model Accuracy, how often is the classifier correct?
print("Accuracy:",metrics.accuracy_score(y_test, y_pred))
