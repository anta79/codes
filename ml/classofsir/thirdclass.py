# %%
import nltk
text = "This is Andrew's text, isn't it?"
# %%
tokenizer = nltk.tokenize.WhitespaceTokenizer()
tokenizer.tokenize(text)
# %%
tokenizer = nltk.tokenize.TreebankWordTokenizer()
tokenizer.tokenize(text)
# %%
tokenizer  = nltk.tokenize.WordPunctTokenizer()
tokenizer.tokenize(text)
# %%
newtext = "feet cats wolves talked corpora better"
tokenizer  = nltk.tokenize.TreebankWordTokenizer()
tokens = tokenizer.tokenize(newtext)
print(tokens)
# %%
stemmer = nltk.stem.PorterStemmer()
" ".join(stemmer.stem(token) for token in tokens)
# %%
# nltk.download('wordnet')
stemmer = nltk.stem.WordNetLemmatizer()
" ".join(stemmer.lemmatize(token) for token in tokens)
