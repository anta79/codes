# %%
from sklearn import datasets
from sklearn import metrics
from sklearn.naive_bayes import GaussianNB
dataset = datasets.load_iris()
print(dataset.target_names)
# %%
model = GaussianNB()
model.fit(dataset.data, dataset.target)
print(model)
# %%
expected = dataset.target
# print(expected)
predicted = model.predict(dataset.data)
# print(predicted)
print(metrics.classification_report(expected,predicted))
print(metrics.confusion_matrix(expected,predicted)) 
