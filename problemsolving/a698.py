def rev(a) :
	if a == 1 :
		return 2 
	return 1 

def solve(n,s):
	prev = 0
	for i in range(n):
		if(s[i] == 3 and prev == 0) :
			k = 0
			for j in range(i,n) :
				k = j - 1
				if(s[j] != 3) :
					break 
			for j in range(k,i-1,-1):
				s[j] = rev(s[j+1])
		elif(i != 0) :
			if(s[i] == 3) :
				s[i] = rev(s[i-1])
			elif(s[i] == 0) :
				s[i] = 0
			else :
				if(s[i] == s[i-1]) :
					s[i] = 0
		prev = s[i]
	return s.count(0)



n = int(input())
li = list(map(int,input().split())) 
print(solve(n,li))
# print(len(li))
