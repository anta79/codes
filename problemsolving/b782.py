import atexit
import io
import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

_OUTPUT_BUFFER = io.StringIO()
sys.stdout = _OUTPUT_BUFFER
@atexit.register
def write():
    sys.__stdout__.write(_OUTPUT_BUFFER.getvalue())


n = int(input())

xarr = list(map(int,input().split()))
varr = list(map(int,input().split())) 

b = max(xarr)
a = min(xarr)

def cal(r):
	t = 0 
	for j in range(n):
		d = abs(r-xarr[j]) / varr[j]
		t = max(t,d)
	return t

while (b-a > 10e-7):
	
	mid1 = a + (b-a) / 3
	mid2 = b - (b-a) / 3

	if(cal(mid1) < cal(mid2)) :
		b = mid2 
	else :
		a = mid1 
print(cal(b))
