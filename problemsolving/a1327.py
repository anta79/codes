r = 0
o = 1
s = 1 
ps = 1
arr = []
while ps <= 10**7:
	r += 1	
	o += 2
	s += o
	for i in range(ps,s):
		arr.append(r)
	ps = s

test = int(input())
for case in range(test):
	ans = False 
	a,b = map(int,input().split())
	if (a % 2 == 0) : 
		if(b % 2 == 0) :
			if( b <= arr[a-1] ) :
				ans = True 
	else :
		if(b % 2 == 1) :
			if(b <= arr[a-1]) :
				ans = True  
	if(ans) :
		print("YES")
	else :
		print("NO")