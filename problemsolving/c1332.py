from collections import defaultdict 
from math import ceil 
from sys import stdin
def solve(st,l,d):
	arr = [] 
	for i in range(0,l,d) :
		arr.append(st[i:(i+d)]) 
	# print(arr) 
	nd = ceil(d / 2)
	# ano = [] 
	ans = 0
	for i in range(nd):
		# ano.append(defaultdict(int))
		m = defaultdict(int) 
		for j in range(l//d):  
			m[arr[j][i]] += 1 
			if (i != (d-(i+1))):
				m[arr[j][d-(i+1)]] += 1 
		ans += sum(m.values()) - max(m.values())
		# print(m)

	return ans

t = int(stdin.readline()) 
lans = ""
for l in range(t):
	l,d = map(int,stdin.readline().split()) 
	st = input() 
	lans += str(solve(st,l,d))
	lans += "\n"
print(lans)
