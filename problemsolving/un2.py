import sys
from collections import Counter


_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__


tc  = int(input()) 

for zzz in range(tc):
	n = int(input()) 
	d = Counter(map(int,input().split()))
	k = list(map(int,d.keys() )) 
	k.sort() 
	rem = 0 
	ans = 0 
	for ele in k :
		t = (d[ele]+rem) // ele 
		ans += t 
		rem = (d[ele]+rem) - t*ele
	print(ans)


