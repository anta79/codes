from bisect import bisect_left 

def fun(arr,k) :
	g = bisect_left(arr,k) 
	if(g == 0) :
		return -1
	if(arr[g] == k) :
		return g 
	g -= 1 
	if(g == 0) :
		return -1 
	return g 


m = 100000
arr = []
arr.append(0)
arr.append(2)
p = 2
for i in range(2,m):
	t = p + (2*i) + (i-1)
	arr.append(t) 
	p = t 
	if(p >= 1000000000) :
		break
# print(arr)

tc = int(input())

for zzz in range(tc):
	k = int(input() )

	g = 0
	ans = 0 
	while (True) :
		g = fun(arr,k)
		if(g == -1) :
			break 
		else :
			# print(arr[g])
			k -= arr[g] 
			ans += 1
	print(ans)