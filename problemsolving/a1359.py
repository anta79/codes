import sys
from math import ceil
_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

def empty(*arg):
	pass 
debug = empty
# debug = print


tc = int(input()) 


for zzz in range(tc):
	n,m,k = map(int,input().split()) 
	b = n // k 
	if(b < m):
		r = m - b
		t = ceil(r / (k-1))
		debug("b = {},t = {}".format(b,t))
		if(t < b):
			print(b-t) 
		else :
			print(0)
	else :
		print(m)