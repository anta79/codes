import sys
_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

tc = int(input()) 

for zzz in range(tc) :
	a,b,c = map(int,input().split()) 

	x = a // c 
	y = b // c  

	if(a % c != 0) :
		x += 1 
	if( b % c != 0) :
		y += 1 
	print(x+y) 