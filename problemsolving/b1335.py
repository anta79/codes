t = int(input()) 

arr = [chr(96+i) for i in range(1,27)]

for l in range(t):
	n,_,k = map(int,input().split()) 
	ans = [] 
	for i in range(n): 
		ans.append(arr[i%k])
	print("".join(ans))
