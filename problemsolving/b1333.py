def test(arr):
	s = len(arr)
	p = 1
	last = -1
	sum = 0
	for i in range(s):
		sum += arr[i] 
		if(((p*(p+1)) // 2) == sum) :
			last = i
		p += 1
	return last 

def solve(li,marked=False):
	arr1 = [] 
	arr2 = [] 
	s = len(li) 
	stop = 0 
	fo = s 
	for i in range(s):
		if (li[i] not in arr1) :
			arr1.append(li[i])
		else :
			stop = i 
			break
	# print(arr1)
	f = test(arr1) 
	if(f < stop) :
		stop = f+1
	# print(stop)
	arr2 = li[stop:s]
	temp = sorted(arr2) 
	# print(arr2)
	done = False 
	if(temp == list(set(arr2))) :
		if(test(arr2) == len(arr2)-1) :
			done = True 
			if(marked) :
				arr2.reverse()
				arr1.reverse()
	nn = []
	if(done) :
		nn.append(arr1)
		nn.append(arr2)		
		return nn
	return [] 


# print(solve([1,2,3,2,1]))
# print(solve([1,2,3,2,1]))
t = int(input()) 
for l in range(t):
	u = int(input()) 
	li = list(map(int,input().split())) 
	rr1 = solve(li) 
	rr2 = solve(li[-1::-1],True)
	rr2.reverse()
	ans = 0
	if(len(rr1) > 0) :
		ans += 1 
	if(len(rr2) > 0) :
		ans += 1 
	if (rr1 == rr2 and ans == 2) :
		ans -= 1 

	print(ans)
	if(ans == 1) :
		if(len(rr2) > 0) :
			print(len(rr2[0]),len(rr2[1]))
		else :
			print(len(rr1[0]),len(rr1[1]))
	elif(ans == 2) :
		print(len(rr1[0]),len(rr1[1]))
		print(len(rr2[0]),len(rr2[1]))
