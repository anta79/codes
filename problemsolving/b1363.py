import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

tc = int(input()) 

for zzz in range(tc):
		s = input() 
		l = len(s)
		sh1 = 0 
		ss1 = 0 
		sh0 = 0 
		ss0 = 0 
		con = 0
		for i in range(0,l):
			if(s[i] == "1"):
				con = i
				break 
		for i in range(con,l):
			if(s[i] == "0") :
				ss1 += 1

		for i in range(l-1,-1,-1):
			if(s[i] == "1"):
				con = i
				break  
		for i in range(con,-1,-1):
			if(s[i] == "0") :
				sh1 += 1 
				
		for i in range(0,l):
			if(s[i] == "0"):
				con = i
				break 
		for i in range(con,l):
			if(s[i] == "1") :
				ss0 += 1

		for i in range(l-1,-1,-1):
			if(s[i] == "0"):
				con = i
				break  
		for i in range(con,-1,-1):
			if(s[i] == "1") :
				sh0 += 1 
		print(min(sh0,sh1,ss1,ss0))