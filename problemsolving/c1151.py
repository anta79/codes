from math import log
def sum1(b):
	if(b == 0):
		return 0
	dhap = int(log(b,2))
	extra = (b - 2**dhap) + 1

	even = odd = 0 

	for i in range(dhap):
		if(i % 2 == 0) : odd += 2**i 
		else : even += 2 ** i 

	if(dhap % 2 == 1) : even += extra
	else : odd += extra  

	return (odd*odd)+even*(even+1)


a,b = map(int,input().split()) 
print((sum1(b) - sum1(a-1)) % (10**9 + 7))
