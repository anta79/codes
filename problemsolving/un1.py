import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__


tc = int(input())

for zzz in range(tc):
	a,k = map(int,input().split()) 
	for i in range(k-1) :
		m = list(map(int,str(a)))
		t = a + max(m)*min(m) 
		a = t 
		if(min(m) == 0) :
			break 
	print(a)