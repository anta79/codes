from collections import Counter

t = int(input())

for l in range(t):
	n = int(input()) 
	li = Counter(map(int,input().split()))
	k = max(li.values()) 
	m = (li.keys())
	if(k >= len(m))  :
		k = min(k-1, len(m))
	print(k)