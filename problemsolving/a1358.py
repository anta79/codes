import sys


_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

tc = int(input()) 

for zzz in range(tc) :
	n,m = map(int,input().split()) 
	l = n*m 
	if(l % 2 == 0) :
		print(l//2)
	else :
		print(l//2 + 1)