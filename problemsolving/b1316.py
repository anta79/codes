from collections import namedtuple

def solve(s):
	l = len(s) 
	m = min(s)
	arr = []  
	pair = namedtuple("pair","value pos")
	for i in range(l) :
		if s[i] == m :
			if(i != l-1) :
				temp1 = s[i:]  
				temp2 = s[0:i] 
				temp = ""
				if ((l-i) % 2 == 1):
					temp = temp1 + temp2[-1::-1]
				else :
					temp = temp1 + temp2 
					
				arr.append(pair(temp,i+1))
			else :
				temp = s[-1::-1]
				arr.append(pair(temp,i+1))

	arr.sort(key=lambda m:m.value)
	# print(arr)
	print(arr[0].value)
	print(arr[0].pos)


# solve("aaaaaaa")

t = int(input())

for l in range(t) :
	_ = int(input()) 
	s = input() 
	solve(s)


