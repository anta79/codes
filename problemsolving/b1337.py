def va(v):
	return (v // 2) + 10

t = int(input())

for l in range(t):
	l,a,b = map(int,input().split()) 

	mn = l 
	temp = l 
	for i in range(a):
		temp = va(temp)
		mn = min(mn,temp)
	if(mn <= b*10) :
		print("YES")
	else :
		print("NO")