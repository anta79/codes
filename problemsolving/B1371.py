import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

tc = int(input()) 

for zzz in range(tc): 
	n,r = map(int,input().split()) 

	ways = 0
	for wd in range(1,r+1) :
		if(n > wd) :
			ways += wd 
		else :
			ways += 1 
			break 
	print(ways)