import atexit
import io
import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

_OUTPUT_BUFFER = io.StringIO()
sys.stdout = _OUTPUT_BUFFER
@atexit.register
def write():
    sys.__stdout__.write(_OUTPUT_BUFFER.getvalue())


tc = int(input()) 

for zzz in range(tc):
	n,k = map(int,input().split()) 
	if(n == 1) :
		print(0) 
	elif(n == 2) :
		print(k) 
	else :
		print(2*k)

