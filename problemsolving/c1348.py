from collections import defaultdict
tc = int(input()) 

for zzz in range(tc) :
	dic = defaultdict(int) 
	n,k = map(int,input().split()) 
	arr = [[] for i in range(k)]
	strn = input() 
	for ele in strn :
		dic[ele] +=  1 
	bal = [chr(i) for i in range(97,97+26)]
	r = 0
	z = 1
	i = 0
	j = 1
	temp = n
	mark = False
	while (True):
		if(temp == 0) :
			break
		if(r >= k or r < 0) :
			z *= -1
			r += z 
		if(i >= 26 or i < 0) :
			j *= -1
			i += j
		if(r == k-1) :
			mark = True 
		# print("i",i)
		if(dic[bal[i]] > 0) :
			arr[r].append(bal[i])
			dic[bal[i]] -= 1
			temp -= 1
			if(dic[bal[i]] > 0 and mark) :
				pass
			else:
				r += z  
		else :
			i += j
	print("".join(max(arr)))
		
