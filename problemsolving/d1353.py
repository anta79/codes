import sys
from collections import deque 
from heapq import * 

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__



def pur(a,b):
	if((b-a+1 )% 2 == 1) :
		m = (b+a) // 2  
	else :
		m = (b+a-1) // 2
	return m 


tc = int(input()) 
for zzz in range(tc) :
	n = int(input()) 
	arr = []
	arr.append(0) 
	for i in range(n) :
		arr.append(0)  
	
	dq1 = deque() 
	dq2 = []

	dq1.append((1,n)) 

	while(len(dq1) > 0):
		tem = dq1.popleft() 
		a = tem[0] 
		b = tem[1] 
		m = pur(a,b)
		dq2.append((-(tem[1]-tem[0]),m))
		if(a != b) :
			if(a != m) :
				dq1.append((a,m-1))
			if(b != m) :
				dq1.append((m+1,b)) 
			

	heapify(dq2) 
	i = 1
	while (len(dq2) > 0):
		r = heappop(dq2)
		d = r[1]
		arr[d] = i 
		i += 1 
		
	for i in range(1,n+1):
		print(arr[i],end = " ")
	print()





