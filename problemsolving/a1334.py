t = int(input()) 

for l in range(t):
	s = int(input())
	arr = [] 
	brr = []
	mark = True 
	for r in range(s):
		a,b = map(int,input().split()) 
		if( b > a) :
			mark = False 
		arr.append(a)
		brr.append(b)
	
	for i in range(1,s):
		if arr[i] < arr[i-1] :
			mark = False 
		if brr[i] < brr[i-1] :
			mark = False 
		if arr[i] - arr[i-1] < brr[i]-brr[i-1]:
			mark = False
	if(mark):
		print("YES")
	else :
		print("NO")
