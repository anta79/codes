import sys
from math import ceil
_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__



tc = int(input()) 
last = []
for zzz in range(tc):

	h,c,t = map(int,input().split()) 

	def so(k):
		n = ceil(k / 2)
		m = k - n 
		# print(n,m)
		return (n*h + m*c) / k 
	prev = 10**18
	ans = 0 
	for i in range(1,100):
		r = so(i)
		# print(r)
		dif = abs(r-t) 
		# print(dif)
		if(dif < prev):
			prev = dif 
			ans = i 
	# print(prev)
	last.append(str(ans))
print("\n".join(last))