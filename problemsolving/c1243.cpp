#include <iostream>
using namespace std ;

int arr[1000001];

int main() {
	arr[0] = arr[1] = 1 ;
	for(int i=2;i<=1002;i++) {
		if(arr[i] == 0) {
			for(int j = i*i; j < 1000001; j+=i ) {
				arr[j] = 1 ;
			}
		}
	}

	int primes[1000001];
	int r = 0 ;
	for(int i=0;i<1000001;i++) {
		if(arr[i] == 0) {
			primes[r] = i ;
			r++ ;
		}
	}

	int n ,ans;
	cin >> n ;
	ans = n ;
	for(int i=0;i<1000001;i++) {
		if(primes[i] == 0) {
			break ;
		}
		if(n % primes[i] == 0) {
			ans = primes[i] ;
		}
	}
	cout << ans ;
}