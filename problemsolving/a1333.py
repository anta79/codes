t = int(input()) 

for l in range(t) :
	si,st = map(int,input().split()) 
	inp  = list(map(int,input().split())) 
	arr = [0] * (max(inp)+1)
	arr[0] = 1 
	for ele in inp :
		arr[ele] = 1 

	sum = ans = 0 
	for i in range(len(arr)):
		if(arr[i] == 0) :
			sum += 1 
			arr[i] = 1
		if(sum == st) :
			break 
	# print(sum)
	if(sum != st):
		for i in range(st-sum):
			arr.append(1)
	# print(arr)
	for i in range(len(arr)):
		if(arr[i] == 0) :
			ans = i - 1 
			break
	if(ans == 0) :
		print(len(arr)-1)
	else :
		print(ans) 