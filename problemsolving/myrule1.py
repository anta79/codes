import atexit
import io
import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

_OUTPUT_BUFFER = io.StringIO()
sys.stdout = _OUTPUT_BUFFER
@atexit.register
def write():
    sys.__stdout__.write(_OUTPUT_BUFFER.getvalue())


n,m,k = map(int,input().split()) 
n += 1 
m += 1 
grid = [[0]*m for i in range(n)]
row = [0]*n
col = [0]*m
inps = []
for i in range(k) :
	inps.append(tuple(map(int,input().split()))) 
box = 0 
for i in range(k-1,-1,-1) :
	if(box == ((n-1)*(m-1))) :
		break 
	if(inps[i][0] == 1) :
		r = inps[i][1]
		if(row[r] == 0) :
			row[r] = inps[i][2]
			for j in range(1,m) :
				if(grid[r][j] == 0) :
					grid[r][j] = inps[i][2]
					box += 1
	else :
		r = inps[i][1]
		if(col[r] == 0) :
			col[r] = inps[i][2]
			for j in range(1,n) :
				if(grid[j][r] == 0) :
					grid[j][r] = inps[i][2]
					box += 1
for i in range(1,n):
	for j in range(1,m):
		print(grid[i][j],end= " ")
	print()
