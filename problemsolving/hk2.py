import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

while(True) :
	try : 
		n,k = map(int,input().split()) 
		v = n 
		i = 1
		su = n
		while(v > 0) :
			v = v // k
			if(i % 2 == 1 and v > 0) :
				v += 1 
			else :
				v -= 1  
			if(v > 0):
				su += v 
			# print(v)
			i += 1 

		print("Total Number of Hawai Misti is : {}".format(su))

	except :
		exit()