import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

def floydWarshall(dist,V): 
    for k in range(V): 
        for i in range(V): 
            for j in range(V): 
                dist[i][j] = min(dist[i][j] ,dist[i][k]+ dist[k][j])

n,k = map(int,input().split()) 
arr = [ [10**10]*n for i in range(n)]
for i in range(k):
	a,b,c = map(int,input().split()) 
	if(arr[a][b] > c ):
		arr[a][b] = c 
		arr[b][a] = c  
for i in range(n):
	arr[i][i] = 0
floydWarshall(arr,n) 
su = 0
qu = int(input())

for i in range(qu):
	a,b = map(int,input().split()) 
	su += arr[a][b] 
print(su) 
