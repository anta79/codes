t = int(input()) 

for l in range(t):
	s = int(input())
	li = list(map(int,input().split()) )
	li.sort() 
	li = li[-1::-1] 
	ans = 1
	for i  in range(1,s):
		h = min(li[i-1],li[i])
		c = min(h,(i+1))
		ans = max(ans,c)
	print(ans) 