import sys
from collections import Counter
_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

tc = int(input()) 
last = []
while(tc) :

	n,k = map(int,input().split()) 
	arr = list(map(int,input().split())) 
	brr = []
	for ele in arr :
		temp = ele % k
		if(temp == 0) :
			brr.append(temp)
		else:
			brr.append(k - temp) 

	fa = Counter(brr)
	# print(fa)
	mas = -1 
	for ele in fa :
		va = fa[ele]
		if(ele == 0) :
			continue  
		mas = max(mas,(k*(va-1)+ele))
	last.append(str(mas+1))
	#
	tc -= 1
print("\n".join(last))