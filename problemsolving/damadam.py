s =  input()
 
pre = [0]*(len(s)+1)
 
for i in range(1,len(s))  :
    pre[i] = pre[i-1]
    if(s[i-1] == s[i]) :
        pre[i] += 1
 
q = int(input())

ans = []
for i in range(q):
    a,b = map(int,input().split())
    ans.append(str(pre[b-1]-pre[a-1]))
print("\n".join(ans))
 