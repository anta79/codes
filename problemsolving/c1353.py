import atexit
import io
import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

_OUTPUT_BUFFER = io.StringIO()
sys.stdout = _OUTPUT_BUFFER
@atexit.register
def write():
    sys.__stdout__.write(_OUTPUT_BUFFER.getvalue())

tc = int(input()) 

for zzz in range(tc) :
	n = int(input()) 

	s = 0 
	p = 1
	for i in range(3,n+1,2) :

		s += ((i**2) - (i-2)**2)*p
		p += 1 
	print(s)