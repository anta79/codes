from collections import namedtuple,Counter
def solve(li):
	n = len(li)
	s = []
	for ele in li :
		s.append(chr(ele+96))
	node = namedtuple("node","x y")
	mainans = 0 
	
	alpha = [chr(i+96) for i in range(1,27)]
	for ele in alpha :
		# for a
		mainans = max(mainans,s.count(ele))
		cur = 0 
		# temp = [] 
		pos = []
		su = 0
		for i in range(n) :
			if(s[i] == ele) :
				cur += 1 
			else :
				if(cur != 0 and s[i] != ele):
					# temp.append(cur)
					pos.append(node(i,cur))
					su += cur 
					cur = 0
				# temp.append(s[i])
		if(cur != 0) :
			# temp.append(cur)
			pos.append(node(n,cur))
			su += cur
		init = 0
		
		left = [0]*(len(pos)+1)
		right = [0]*(len(pos)+1)
		left[0] = 0
		right[0] = su 
		for i in range(1,len(pos)+1) :
			left[i] = left[i-1] + pos[i-1].y
			right[i] = right[i-1] - pos[i-1].y

		# inmax = s.count(ele)

		for i in range(len(pos)) :
			# print(s[init:(pos[i].x-pos[i].y)])
			lulu = s[init:(pos[i].x-pos[i].y)]

			migians = 0
			if(len(lulu) > 0) :
				temp100 = Counter(lulu) 
				migians = max(temp100.values())

			migians += (2*min(left[i],right[i]))
			# print("left {} right {}".format(left[i],right[i]))
			mainans = max(migians,mainans)

			init = pos[i].x

		if(init != n) :
			lulu = s[init:n]
			temp100 = Counter(lulu) 
			migians = max(temp100.values())
			mainans = max(migians,mainans)

	# print(temp)
	print("%d" %mainans)
	# print(pos)
	# print(left)
	# print(right)

t = int(input()) 

for l in range(t):
	_ = int(input())
	li = list(map(int,input().split())) 
	solve(li)
