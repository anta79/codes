from math import gcd,ceil

def work(z,lcm,v_nums):
	m = z // lcm  
	m += 1 
	temp = m*lcm 
	tran = temp - v_nums 
	ans = (m-1)*v_nums  
	if(tran <= z) :  
		ans += (z-tran)+1 
	return ans

# for i in range(110):
# 	print(i,work(i,14,7))

t = int(input()) 

for ll in range(t):
	a,b,q  = map(int,input().split()) 
	lcm = (a*b) // gcd(a,b)
	v_nums = lcm - max(a,b) 
	for llb in range(q):
		ans = 0 
		r,z = map(int,input().split()) 
		ans += work(z,lcm,v_nums)
		ans -= work(r-1,lcm,v_nums)
		print(ans,end=' ')
	print()

