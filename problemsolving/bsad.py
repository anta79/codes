import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

t = int(input()) 
s = "codeforces"
arr = [1]*10
cur = 1 
i = 0
while(cur < t) :
	pos = i % 10
	arr[pos] += 1
	cur  = 1 
	for j in range(10) :
		cur *= arr[j]
	i += 1
# print(arr)

ans = [""]
for i in range(10):
	ans.append(s[i]*arr[i])
print("".join(ans))