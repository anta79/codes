import sys
from collections import defaultdict

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

def empty(*arg):
	pass 
# debug = empty
debug = print

tc = int(input()) 

for zzz in range(tc):
	n,k = map(int,input().split()) 
	arr = [defaultdict(int) for i in range(n)]
	for i in range(n) :
		s = input() 
		for ele in s :
			arr[i][ele] += 1 
	debug(arr)