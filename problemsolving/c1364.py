import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

n = int(input()) 

arr= list(map(int,input().split())) 
mark = True 
brr = [0]*n
if(arr[0] == 0):
	brr[0] = 1 
	for i in range(1,n):
		if(arr[i] == 1) :
			mark = False
for i in range(1,n):
	if(arr[i] > i+1) :
		# print("bal")
		mark = False 

if(not mark):
	print(-1)

else :
	maxi = arr[-1]-1
	empty = [True]*(maxi+1)
	empty[brr[0]] = False 

	for i in range(n-1,0,-1): 
		if(arr[i] != arr[i-1]):
			brr[i] = arr[i-1]
			# print("arr[i]",arr[i-1])
			empty[arr[i-1]] = False
		
		else :

			while(not empty[maxi]):
				maxi -= 1
				if(maxi == -1) :
					maxi = brr[0]
					break 
			brr[i] = maxi 
			# empty[maxi] = False 
	
	print(" ".join(map(str,brr)))

