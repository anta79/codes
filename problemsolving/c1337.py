from collections import deque,namedtuple
from heapq import * 
n,k = map(int,input().split()) 
li = []


li.append([])
for i in range(n) :
	li.append([])


for i in range(n-1) :
	u,v = map(int,input().split())
	li[u].append(v)
	li[v].append(u)


# node = namedtuple("node","value depth")
nodes = [] #size n
nodes.append((0,1))

# print(nodes)
visited = [False] * (n+1)
parent = [0] * (n+1)
childs = [0] * (n+1)
depth = [0]*(n+1)

dq = deque()
dq.appendleft(1) 
parent[1] = 1

while(len(dq) > 0) :
	un = dq.popleft()
	visited[un] = True
	for ele in li[un] :
		if(visited[ele] == False):
			depth[ele] = depth[un] + 1
			parent[ele] = un
			dq.appendleft(ele)
			nodes.append((-depth[ele],ele)) 
			visited[ele] = True

heapify(nodes)

# nodes.sort(key = lambda x: -x.depth)
# print(nodes)
# print(parent)
nnodes = [0]*n
for i in range(n) :
	# temp = ele.value 
	tup = heappop(nodes)
	nnodes[i] = tup[1]  
	temp = tup[1] 
	if(temp != parent[temp]) : 
		childs[parent[temp]] += childs[temp] + 1

bara = []
# print(nnodes)
# pods = namedtuple("pods","value cc")
for ele in nnodes :
	# p = pods(nodes[i].value,depth[nodes[i].value] - childs[nodes[i].value])
	bara.append(-1*(depth[ele]-childs[ele]))

# print(bara)
heapify(bara)
# bara.sort(key=lambda x:x.cc)
# print(bara)

# for ele in bara :
# 	temp = ele.value 
# 	if(temp != parent[temp]) : 
# 		childs[parent[temp]] += childs[temp] + 1
# print(childs)
lulu = 0
for i in range(k):
	# lulu += nodes[i].depth 
	# lulu -= childs[nodes[i].value]
	lulu += (-1*heappop(bara))
	
print(lulu)