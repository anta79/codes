from collections import deque,defaultdict
from math import ceil

def fun(arr):
	print(''.join(arr))

tc = int(input()) 

for zzz in range(tc):
	dic = defaultdict(int)
	n,k = map(int,input().split()) 
	strn = input() 
	for ele in strn : 
		dic[ele] += 1
	brr = [str(i) for i in dic.keys()]
	w = deque()
	brr.sort()
	arr = [[] for i in range(k)]
	for ele in brr:
		p = dic[ele]
		for i in range(p):
			w.append(ele)
	# print(w)
	for i in range(k):
		arr[i].append(w.popleft())

	if(dic[brr[0]] < k) :
		fun(arr[k-1])
	elif(len(set(w)) > 1):
		while(w):
			arr[k-1].append(w.popleft())
		fun(arr[k-1])
	else :
		r = 0
		while(w):
			arr[r%k].append(w.popleft())
			r += 1
		fun(max(arr))

			