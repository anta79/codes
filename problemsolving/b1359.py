import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

tc= int(input()) 

for zzz in range(tc):
	r,c,x,y = map(int,input().split()) 
	arr = [None]*r 
	for i in range(r) :
		arr[i] = input() 
	low = min(2*x,y)  
	su = 0 
	for i in range(r):
		j = 0
		while(j<c) :
			if(arr[i][j] == ".") :
				if(j+1 < c):
					if(arr[i][j+1] == ".") :
						su += low 
					else :
						su += x
				else :
					su += x
				j += 2 
			else :
				j += 1
	print(su)