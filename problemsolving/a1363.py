import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

tc = int(input()) 

for zzz in range(tc):
	n,k = map(int,input().split()) 
	a = b = 0 
	arr = list(map(int,input().split())) 
	for ele in arr :
		if(ele % 2 == 0) :
			b += 1 
		else :
			a += 1
	can = False  
	# print(a)
	for i in range(1,a+1,2):
		r = k - i 
		# print(r)
		if(-1 < r and r <= b) :
			can = True 
			break 
	if(can):
		print("Yes")
	else :
		print("No")