import sys
import bisect

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

def empty(*arg):
	pass 
# debug = empty
debug = print

a,b,c = map(int,input().split()) 

arr = list(map(int,input().split()))

k = bisect.bisect_left(arr,3)

print(k)