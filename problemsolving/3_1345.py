from collections import defaultdict
tc = int(input()) 

for zzz in range(tc):
	n = int(input()) 
	arr = list(map(int,input().split()))  
	s = defaultdict(int)
	for i in range(1,n+1):
		t = i + arr[(i%n)] 
		s[t] += 1
	p = True
	for i in s.keys() :
		if(s[i] > 1) :
			p = False
	if(p):
		print("YES")
	else :
		print("NO")
