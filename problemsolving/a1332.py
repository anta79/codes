t = int(input())

for i in range(t):
	a,b,c,d = map(int,input().split())
	x,y,x1,y1,x2,y2 = map(int, input().split())
	
	ans1 = False
	ans2 = False

	rs = abs(x2-x)
	us = abs(y2 -y)
	ds = abs(y-y1)
	ls = abs(x - x1)
 
	if(a > b) :
		if(ls >= (a-b)) : 
			ans1 = True
	else:
		if((b-a) <= rs) : 
			ans1 = True

	if(d > c) :
 		if (us >= (d-c)) : 
 			ans2 = True
	else :
 		if(ds >= (c-d)) :  
 			ans2 = True

	if(rs==0 and ls == 0) :
 		if(a > 0 or b > 0) :
 			ans1 = False
	if(us==0 and ds == 0) :
 		if(c > 0 or d > 0) :
 			ans2 = False
	if(ans2 and ans1) :
		print("Yes")
	else :
		print("No")