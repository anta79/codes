import atexit
import io
import sys

_INPUT_LINES = sys.stdin.read().splitlines()
input = iter(_INPUT_LINES).__next__

_OUTPUT_BUFFER = io.StringIO()
sys.stdout = _OUTPUT_BUFFER
@atexit.register
def write():
    sys.__stdout__.write(_OUTPUT_BUFFER.getvalue())

tc = int(input()) 

for zzz in range(tc) :
	n,k = map(int,input().split()) 
	arr = list(map(int,input().split())) 
	brr = list(map(int,input().split())) 
	arr.sort()
	brr.sort() 
	crr = brr[-1::-1] 
	mar = [False]*n 

	for i in range(n) :
		if(k == 0) :
			break 
		for j in range(n):
			if(arr[i] < crr[j] and mar[j] == False) :
				arr[i] = crr[j] 
				mar[j] = True 
				k -= 1
				break 
	print(sum(arr))