#include <bits/stdc++.h>
#define  nl "\n"
#define forn(i, n) for (int i = 0; i < int(n); i++)
typedef long long ll ;

using namespace std ;

int main() {
    ios_base::sync_with_stdio(false) ;
    cin.tie(NULL) ;
    
    int tc ;
    cin >> tc ;

    while(tc--) {
        int n ;
        cin >> n ;
        vector <int> arr(n) ;
        forn(i,n) {
             cin >> arr[i] ;
        }
        bool des = 1 ;
        int m = *max_element(arr.begin(),arr.end()) ;
        if(n != m) {
            des = 0 ;
        } 
        // cout << des << nl ;
        forn(i,n-1) {
            if(arr[i] > arr[i+1]) {
                des = 0 ;
            }
            if(arr[i] < i+1) {
                des = 0 ;
            }
        }
        // cout << des << nl ;
        if(!des) {
            cout << "-1" << nl ;
        }
        else {
            int map[n+1] ;
            forn(i,n+1) {
                map[i] = 0 ;
            }
            vector <int> ans ;
            int low = 1 ;
            for(auto ele : arr) {
                if(!map[ele]) {
                    ans.push_back(ele) ;
                    map[ele] = 1 ;
                    if(ele == low) {
                        low += 1  ;
                    }
                }
                else {
                    while(map[low]) {
                        low += 1 ;
                    }
                    ans.push_back(low) ;
                    map[low] = 1 ;
                    low += 1 ;
                }
            }  
            for(auto ele : ans) {
                cout << ele << " " ;
            }      
            cout << nl ;   
        }
    }
}