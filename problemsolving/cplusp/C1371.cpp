#include <bits/stdc++.h>
#define  nl "\n"
#define forn(i, n) for (int i = 0; i < int(n); i++)
typedef long long ll ;

using namespace std ;

int main() {
    ios_base::sync_with_stdio(false) ;
    cin.tie(NULL) ;
    
    int tc ;
    cin >> tc; 
    while(tc--) {
        ll a,b,n,m ;
        cin >> a >> b  >> n >> m ;
        if(a > b) {
            swap(a,b) ;
        }
        ll rem = a - m ;
        if(rem < 0) {
            cout << "NO" << nl ;
            continue ;
        }
        ll extra = rem +  b ; 
        if(extra < n)  {
            cout << "NO" << nl ;
        }       
        else {
            cout << "YES" << nl ;
        }
    }
}