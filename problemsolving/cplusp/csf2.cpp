#include <bits/stdc++.h>
#define  nl "\n"
#define forn(i, n) for (int i = 0; i < int(n); i++)
typedef long long ll ;

using namespace std ;

int main() {
    ios_base::sync_with_stdio(false) ;
    cin.tie(NULL) ;
    

    int tc ;
    cin >> tc ;
    while(tc--) {
        int n,a,b,ansx,ansy ;
        map <int,int> x ;
        map<int,int> y ;
        cin >> n ;
        forn(i,4*n-1) {
            cin >> a >> b ;
            x[a] += 1 ;
            y[b] += 1 ;
        }

        for(auto ele : x) {
            if(ele.second % 2 == 1) {
                ansx = ele.first ;  
                break ;         
            }
        }
        for(auto ele:y ) {
            if(ele.second %2 == 1) {
                ansy = ele.first ;
                break ;
            }
        }
        cout << ansx << " " << ansy << nl ;
    }
}