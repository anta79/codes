#include <bits/stdc++.h>
#define  nl "\n"
#define forn(i, n) for (int i = 0; i < int(n); i++)
typedef long long ll ;

using namespace std ;

int main() {
    ios_base::sync_with_stdio(false) ;
    cin.tie(NULL) ;
    
    int n ;
    cin >> n ;

    string k ; 
    cin >> k ;
    int w,b ;
    w = b = 0 ;
    forn(i,n) {
        if(k[i] == 'B') {
            b += 1 ;
        }
        else {
            w += 1 ;
        }
    }
    char t ,p;
    if(w % 2 == 0 || b % 2 == 0) {
        if(w % 2 == 1) {
            t = 'W' ;
            p = 'B' ;
        }
        else  {
            t = 'B' ;
            p = 'W' ;
        }
        // cout << t << " " << p ;
        vector <int> ans ;
        while(1) {
            int extra = 0 ;
            forn(i,n) {
                if(k[i] == p) {
                    extra += 1 ;
                }
            }
            if(extra == 0) {
                break ;
            }
            forn(i,n-1) {
                if(k[i] == p) {
                    ans.push_back(i+1) ;
                    k[i] = t  ;
                    if(k[i+1] == t) { k[i+1] = p ; }
                    else {k[i+1] = t ; }
                }
            }
        }
        cout << ans.size() <<nl ;
        for(auto v : ans) {
            cout << v << " " ;
        }
    }
    else {
        cout << -1 << nl ;
    }
}