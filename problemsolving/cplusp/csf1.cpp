#include <bits/stdc++.h>
#define  nl "\n"
#define forn(i, n) for (int i = 0; i < int(n); i++)
typedef long long ll ;

using namespace std ;

int main() {
    ios_base::sync_with_stdio(false) ;
    cin.tie(NULL) ;
    
    int tc ;
    cin >> tc ;
    while(tc--) {
        int n ;
        cin >> n ;
        ll ans = 0L ; 
        int arr[n] ;
        forn(i,n) {
            cin >> arr[i] ;
        }
        for(int i=1;i<n;i++) {
            int temp = abs(arr[i]-arr[i-1]) ;
            if(temp < 2) {
                ans += 0 ;
            } 
            else {
                ans += temp-1 ;
            }
        }
        cout << ans << nl ;
    }
}