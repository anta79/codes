#include <bits/stdc++.h>
#define  nl "\n"
#define forn(i, n) for (int i = 0; i < int(n); i++)
typedef long long ll ;

using namespace std ;

int main() {
    int tc ;
    scanf("%d",&tc) ;

    while(tc--) {
        int n,k ;
        scanf("%d %d",&n,&k) ;
        int arr[n] ;
        int brr[n] ;

        forn(i,n) scanf("%d",&arr[i]) ;
        forn(i,n) {
            int temp = arr[i] % k ;
            if(temp == 0) {
                brr[i] = temp ;
            }
            else {
                brr[i] = k - temp ;
            }
        }
        map <int,int> counter ;
        forn(i,n) {
            counter[brr[i]] += 1 ;
        }
        ll mas = -1 ;
        for(auto ele : counter) {
            int key = ele.first ;
            ll val = ele.second ;

            if(key == 0) {
                continue ;
            } 
            mas = max(mas,k*(val-1)+key) ; 
        }
        printf("%lld\n",mas+1) ;
    }
}