#include <bits/stdc++.h>
#define  nl "\n"
#define forn(i, n) for (int i = 0; i < int(n); i++)
typedef long long ll ;

using namespace std ;

int main() {
    ios_base::sync_with_stdio(false) ;
    cin.tie(NULL) ;
    
    int n ;
    cin >> n ;
    ll arr[n] ;
    forn(i,n) {
        cin >> arr[i] ;
    }
    ll dp[n] ;
    dp[0] = 0 ;
    dp[1] = abs(arr[1]-arr[0]) ;

    for(int i=2;i<n;i++) {
        dp[i] = min(dp[i-1]+abs(arr[i]-arr[i-1]),dp[i-2]+abs(arr[i]-arr[i-2])) ;
    }
    cout << dp[n-1] << nl ;
}