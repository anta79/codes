#include <bits/stdc++.h>
#define  nl "\n"
#define forn(i, n) for (int i = 0; i < int(n); i++)
typedef long long ll ;

using namespace std ;

int main() {
    ios_base::sync_with_stdio(false) ;
    cin.tie(NULL) ;
    
    int n ,a,b,c;
    cin >> n ;
    int ans[3] ;
    int cur[3] ;

    forn(i,n) {
        cin >> cur[0] >> cur[1] >> cur[2] ; 
        if(i==0) {
            ans[0] = cur[0] ; 
            ans[1] = cur[1] ;
            ans[2] = cur[2] ; 
            continue ;
        }
        a = ans[0] ; b = ans[1] ; c= ans[2] ;
        ans[0] = max(cur[0]+b,cur[0]+c);
        ans[1] = max(cur[1]+a,cur[1]+c);
        ans[2] = max(cur[2]+b,cur[2]+a);
    }
    cout << *max_element(ans,ans+3) << nl;
}