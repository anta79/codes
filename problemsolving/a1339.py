import math
t = int(input()) 

for l in range(t):
	n = int(input()) 
	li = list(map(int,input().split()))
	brr = [] 
	cmax = li[0] 
	for i in range(1,n) :
		if(cmax > li[i]) :
			brr.append(cmax-li[i]) 
		cmax = max(cmax,li[i])
	# print(brr)
	if(len(brr) == 0) :
		print(0)
	else :
		k = max(brr) + 1 
		ans = math.ceil(math.log2(k) ) 
		print(int(ans))



