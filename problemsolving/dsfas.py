n = int(input()) 

arr = [0]*100001
arr[0] = 1
for i in range(1,100000+1) :
  arr[i] = arr[i-1]*i % 1000000007 

arr[1] = 0 
arr[2] = 0  
# print(arr)
for tc in range(1,n+1) :
  m,l = map(int,input().split()) 
  print("Case {}: {}".format(tc,arr[m]))