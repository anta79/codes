from collections import defaultdict
a,b = map(int,input().split())
li = list(map(int,input().split()))

arr = [0]*a 

for i in range(a):
	arr[i] = defaultdict(int)

temp = li[0]
arr[0][temp] += 1 

for i in range(1,a) :
	arr[i] = arr[i-1].copy()
	temp = li[i]
	arr[i][temp] += 1 
# for ele in arr :
# 	print(ele)


