	/* lexer specification file for calculator 3 */
%{
#include <stdlib.h>
#include "calc3.h"
#include "calc3parser.h"

void yyerror(const char *);
%}

%option yylineno noyywrap

%%

[a-z]       { 
                yylval.sIndex = *yytext - 'a';
                return VARIABLE;
            }

0           {
                yylval.iValue = atoi(yytext);
                return INTEGER;
            }

[1-9][0-9]* {
                yylval.iValue = atoi(yytext);
                return INTEGER;
            }

[-()<>=+*/;{}.] {
                return *yytext;
             }

">="            return GE;
"<="            return LE;
"=="            return EQ;
"!="            return NE;
"while"         return WHILE;
"if"            return IF;
"else"          return ELSE;
"print"         return PRINT;

[ \t\n]+        {    /* ignore whitespace, just update line number for '\n' */
					if(*yytext=='\n')
					yylineno = yylineno+1; 
				}       

.               yyerror("Unknown character");
%%

