  /* calculator #2 lexer specification */
%{
    #include "calc2parser.h"
    #include <stdlib.h>
    void yyerror(const char *);
%}

%option noyywrap yylineno

%%

[a-z]       {
                yylval = *yytext - 'a';
                return VARIABLE;
                }

[0-9]+      {
                yylval = atoi(yytext);
                return INTEGER;
            }

[-+()=/*\n]     { if(*yytext=='\n')
						yylineno = yylineno+1;
					return *yytext; }

[ \t]   ;       /* skip whitespace */

.               yyerror("Unknown character");

%%


