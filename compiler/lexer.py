from rply import LexerGenerator 

lg = LexerGenerator() 
lg.add('Number', r'\d+')
lg.add('Plus', r'\+')
lg.add('Minus', r'-')
lg.ignore(r'\s+')

l = lg.build() 

for token in l.lex(input()) :
    print(token)