import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.StringTokenizer;

class life {
    public static void main(String[] args) throws  Exception{
        Reader sc = new Reader("");
        /*--------------------------*/
        Pair[] arr = new Pair[3] ;
        arr[0] = new Pair(4,2);
        arr[1] = new Pair(5,1);
        arr[2] = new Pair(6,0);
        Arrays.sort(arr,(a,b)->a.y-b.y);
        Arrays.stream(arr).forEach(i -> pr.print(i.x));
        /*--------------------------*/
        pr.close();
    }

    static PrintWriter pr = new PrintWriter(System.out);
    static class Pair {
        int x;
        int y ;
        Pair(int a,int b) {
            x = a ; y = b ;
        }
    }
    static class Reader {
        BufferedReader br;
        StringTokenizer st;
        public Reader() {
            br = new BufferedReader(new
                    InputStreamReader(System.in));
        }
        public Reader(String k) throws Exception {
            br = new BufferedReader(new FileReader(new File("input")));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
            return Integer.parseInt(next());
        }
        long nextLong() {
            return Long.parseLong(next());
        }
        double nextDouble() {
            return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}