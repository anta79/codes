import java.io.*;
import java.util.StringTokenizer;

public class B1370 {
    public static void main(String[] args) throws  Exception{
        Reader sc = new Reader("");
        /*--------------------------*/
        int tc = sc.nextInt() ;
        while (tc-- > 0) {
            int n = sc.nextInt() ;
            int arr[] = new int[2*n] ;
            int jor,bij ;
            jor = bij = 0 ;
            for (int i = 0; i < 2*n; i++) {
                arr[i] = sc.nextInt() ;
                if(arr[i] % 2 == 0) {
                    jor += 1 ;
                }
                else {
                    bij += 1 ;
                }
            }
            int jarr[] = new int[jor] ;
            int barr[] = new int[bij] ;
            int k, j ;
            k = j = 0 ;
            for (int i = 0; i < 2*n; i++) {
                if(arr[i] % 2 == 0) {
                    jarr[k++] = i+1 ;
                }
                else {
                    barr[j++] = i+1 ;
                }
            }

            if(jor % 2 == 0) {
                int moha = 0 ;
                for(int i=0;i<jor;i+=2) {
                    if(moha == n-1) {
                        break ;
                    }
                    moha += 1 ;
                    pr.println(jarr[i]+ " "+jarr[i+1]);
                }
                for(int i=0;i<bij-2;i+=2) {
                    pr.println(barr[i]+ " "+barr[i+1]);
                }
            }
            else {
                for(int i=0;i<jor-1;i+=2) {
                    pr.println(jarr[i]+ " "+jarr[i+1]);
                }
                for(int i=0;i<bij-1;i+=2) {
                    pr.println(barr[i]+ " "+barr[i+1]);
                }
            }

        }
        /*--------------------------*/
        pr.close();
    }

    static PrintWriter pr = new PrintWriter(System.out);
    static class Reader {
        BufferedReader br;
        StringTokenizer st;
        public Reader() {
            br = new BufferedReader(new
                    InputStreamReader(System.in));
        }
        public Reader(String k) throws Exception {
            br = new BufferedReader(new FileReader(new File("input")));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
            return Integer.parseInt(next());
        }
        long nextLong() {
            return Long.parseLong(next());
        }
        double nextDouble() {
            return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}