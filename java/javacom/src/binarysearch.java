import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringTokenizer;

class binarysearch {
    public static void main(String[] args) throws  Exception{
        Reader sc = new Reader("");
        /*--------------------------*/

        int[] arr = {1,1,1,2,4,6,7,7,7,7,7,7,7,8} ;
        ArrayList<Integer> v= new ArrayList<>() ;
        Arrays.stream(arr).forEach(i-> v.add(i));
        pr.println(Collections.binarySearch(v,1));

        /*--------------------------*/
        pr.close();
    }

    static PrintWriter pr = new PrintWriter(System.out);
    static class Reader {
        BufferedReader br;
        StringTokenizer st;
        public Reader() {
            br = new BufferedReader(new
                    InputStreamReader(System.in));
        }
        public Reader(String k) throws Exception {
            br = new BufferedReader(new FileReader(new File("input")));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
            return Integer.parseInt(next());
        }
        long nextLong() {
            return Long.parseLong(next());
        }
        double nextDouble() {
            return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}