import java.io.*;
import java.util.StringTokenizer;

public class C1370 {
    public static void main(String[] args) throws  Exception{
        Reader sc = new Reader("");
        /*--------------------------*/
        int tc= sc.nextInt() ;
        while (tc-- > 0) {
            int n = sc.nextInt() ;
            int m = 5 ;
            boolean[] arr = {false,false,true,true,false} ;
            boolean ans ;
            if(n < 5) {
                ans = arr[n] ;
            }
            else {
                if(n % 2 == 1) {
                    ans = true ;
                }
                else {
                    int tm ;
                    if(if2pow(n)) {
                        ans = false ;
                    }
                    tm = n / 2 ;
                    if(tm % 2 == 1) {
                        ans = false ;
                    }
                    else {
                        ans = true ;
                    }
                }
            }
            if(ans) {
                pr.println("Ashishgup");
            }
            else {
                pr.println("FastestFinger");
            }
        }
        /*--------------------------*/
        pr.close();
    }
    public static boolean if2pow(int n) {
        for(int i=2;;i++) {
            double k = Math.pow(2,i) ;
            int con = (int) k ;
            if(con > n) {
                return  false ;
            }
            if(con == n) {
                return true ;
            }
        }
    }
    static PrintWriter pr = new PrintWriter(System.out);
    static class Reader {
        BufferedReader br;
        StringTokenizer st;
        public Reader() {
            br = new BufferedReader(new
                    InputStreamReader(System.in));
        }
        public Reader(String k) throws Exception {
            br = new BufferedReader(new FileReader(new File("input")));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
            return Integer.parseInt(next());
        }
        long nextLong() {
            return Long.parseLong(next());
        }
        double nextDouble() {
            return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}