import java.io.*;
import java.util.StringTokenizer;

public class A1369 {
    public static void main(String[] args) throws  Exception{
        Reader sc = new Reader("");
        /*--------------------------*/

        int tc = sc.nextInt() ;
        while(tc-- >0) {
            int n = sc.nextInt() ;
            if(n == 3) {
                pr.println("NO");
            }
            else if(n%4 == 0) {
                pr.println("YES");
            }
            else {
                pr.println("NO");
            }
        }

        /*--------------------------*/
        pr.close();
    }

    static PrintWriter pr = new PrintWriter(System.out);
    static class Reader {
        BufferedReader br;
        StringTokenizer st;
        public Reader() {
            br = new BufferedReader(new
                    InputStreamReader(System.in));
        }
        public Reader(String k) throws Exception {
            br = new BufferedReader(new FileReader(new File("input")));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
            return Integer.parseInt(next());
        }
        long nextLong() {
            return Long.parseLong(next());
        }
        double nextDouble() {
            return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}