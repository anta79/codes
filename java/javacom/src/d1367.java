import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;

class d1367 {
    public static void main(String[] args) throws  Exception{
        Reader sc = new Reader("");
        /*--------------------------*/
        int tc= sc.nextInt() ;

        for(int zz=0;zz<tc;zz++) {
            String s = sc.nextLine() ;
            boolean mark[] = new boolean[26] ;
            ArrayList<Character> arr = new ArrayList<>();
            int l = s.length() ;
            for (int i = 0; i < l; i++) {
                int t = (int) s.charAt(i) - 97 ;
                mark[t] = true ;
            }
            for (int i = 0; i <26 ; i++) {
                if(mark[i]) {
                    arr.add((char) (97+i));
                }
            }
//            pr.println(arr);
            int siz = arr.size() ;
            int n = sc.nextInt() ;
            Pair[] parr = new Pair[n] ;
            for (int i = 0; i < n; i++) {
                parr[i] = new Pair(sc.nextInt(),i);
            }
            Arrays.sort(parr,(x,y)-> y.a- x.a);
//            Arrays.stream(parr).forEach(i-> pr.println(i.a + " "+i.b));
//            pr.println(parr);
            char[] ans = new char[n] ;
            ans[parr[0].b] = arr.get(0) ;
            int k = 0 ;
            for (int i = 1; i < n; i++) {
                if(parr[i].a != parr[i-1].a) {
                    k++ ;
                    if(k == siz) {
                        k-- ;
                    }
                }
                ans[parr[i].b] = arr.get(k) ;
            }
            pr.println(ans);
        }


        /*--------------------------*/
        pr.close();
    }
    static class  Pair {
        int a,b ;
        Pair(int x,int y) {
            a = x ;
            b = y ;
        }
    }
    static PrintWriter pr = new PrintWriter(System.out);
    static class Reader {
        BufferedReader br;
        StringTokenizer st;
        public Reader() {
            br = new BufferedReader(new
                    InputStreamReader(System.in));
        }
        public Reader(String k) throws Exception {
            br = new BufferedReader(new FileReader(new File("input")));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
            return Integer.parseInt(next());
        }
        long nextLong() {
            return Long.parseLong(next());
        }
        double nextDouble() {
            return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}