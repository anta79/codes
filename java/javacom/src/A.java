import java.io.*;
import java.util.StringTokenizer;

public class A {
    public static void main(String[] args) throws  Exception{
        Reader sc = new Reader("");
        /*--------------------------*/

        int tc = sc.nextInt() ;
        while (tc-- > 0) {
            int n,m ;
            n = sc.nextInt() ;
            m = sc.nextInt() ;
            int[] sar = new int[n] ;
            int[] col = new int[m] ;
            int[][] arr = new int[n][m] ;
            for(int i=0;i<n;i++) {
                for(int j =0;j<m;j++) {
                    arr[i][j] = sc.nextInt() ;
                    if(arr[i][j] == 1) {
                        sar[i] = 1 ;
                        col[j] = 1 ;
                    }
                }
            }
            int ans = 0 ;
            for(int i=0;i<n;i++) {
                if(sar[i] == 0) {
                    for(int j=0;j<m;j++) {
                        if(col[j] == 0) {
                            arr[i][j] = 1 ;
                            sar[i] = 1 ;
                            col[j] = 1 ;
                            ans++ ;
                            break ;
                        }
                    }
                }
            }
            if(ans % 2 == 0) {
                pr.println("Vivek");
            }
            else {
                pr.println("Ashish");
            }
        }

        /*--------------------------*/
        pr.close();
    }

    static PrintWriter pr = new PrintWriter(System.out);
    static class Reader {
        BufferedReader br;
        StringTokenizer st;
        public Reader() {
            br = new BufferedReader(new
                    InputStreamReader(System.in));
        }
        public Reader(String k) throws Exception {
            br = new BufferedReader(new FileReader(new File("input")));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
            return Integer.parseInt(next());
        }
        long nextLong() {
            return Long.parseLong(next());
        }
        double nextDouble() {
            return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}