import java.io.*;
import java.util.StringTokenizer;

class B {
    public static void main(String[] args) throws  Exception{
        Reader sc = new Reader("");
        /*--------------------------*/
        int tc = sc.nextInt() ;
        while(tc-- > 0) {
            boolean mark1, mark2, ok;
            mark1 = mark2 = false;
            ok = true;
            int n = sc.nextInt();
            int arr[] = new int[n];
            int brr[] = new int[n];

            for (int i = 0; i < n; i++) {
                arr[i] = sc.nextInt();
                if (i > 0) {
                    if (arr[i] < arr[i - 1]) {
                        ok = false;
                    }
                }
            }
            for (int i = 0; i < n; i++) {
                brr[i] = sc.nextInt();
                if (brr[i] == 0) {
                    mark1 = true;
                } else if (brr[i] == 1) {
                    mark2 = true;
                }
            }
            if (ok || (mark1 && mark2)) {
                pr.println("Yes");
            } else {
                pr.println("No");
            }
        }
        /*--------------------------*/
        pr.close();
    }

    static PrintWriter pr = new PrintWriter(System.out);
    static class Reader {
        BufferedReader br;
        StringTokenizer st;
        public Reader() {
            br = new BufferedReader(new
                    InputStreamReader(System.in));
        }
        public Reader(String k) throws Exception {
            br = new BufferedReader(new FileReader(new File("input")));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
            return Integer.parseInt(next());
        }
        long nextLong() {
            return Long.parseLong(next());
        }
        double nextDouble() {
            return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}