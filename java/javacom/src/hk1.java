import java.io.*;
import java.util.Deque;
import java.util.LinkedList;
import java.util.StringTokenizer;

class hk1 {
    public static void main(String[] args) throws  Exception{
        Reader sc = new Reader("");
        /*--------------------------*/
        LinkedList<String> dk = new LinkedList<>();
        int n = sc.nextInt() ;

        for (int i = 0; i < n; i++) {
            int type,num ;
            String line  = sc.nextLine() ;
            if(line.length() == 1) {
                type = Integer.valueOf(line);
                if(type == 2) {
                    dk.pollFirst();
//                    pr.println(dk);
                }
                else{
                    pr.print(dk.peekFirst()+"\n");
                }
            }
            else {
                dk.addLast(line.substring(1));
//                pr.println(dk);
            }
        }

        /*--------------------------*/
        pr.close();
    }

    static PrintWriter pr = new PrintWriter(System.out);
    static class Reader {
        BufferedReader br;
        StringTokenizer st;
        public Reader() {
            br = new BufferedReader(new
                    InputStreamReader(System.in));
        }
        public Reader(String k) throws Exception {
            br = new BufferedReader(new FileReader(new File("input")));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
            return Integer.parseInt(next());
        }
        long nextLong() {
            return Long.parseLong(next());
        }
        double nextDouble() {
            return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}