import java.io.*;
import java.util.*;

class C {
    public static void main(String[] args) throws  Exception{
        Reader sc = new Reader("");
        /*--------------------------*/

        int n = sc.nextInt() ;
        HashMap<Integer,Integer> arr = new HashMap<>();
        HashMap<Integer,Integer> brr = new HashMap<>() ;

        for(int i=1;i<=n;i++) {
            int a = sc.nextInt() ;
            arr.put(a,i) ;
        }
        for (int i = 1; i <=n ; i++) {
            int a = sc.nextInt() ;
            brr.put(a,i);
        }
        HashMap<Integer,Integer> blal = new HashMap<>() ;
        blal.put(0,0);
        
        arr.forEach((k,v)-> {
              int  x = v ;
              int  y = brr.getOrDefault(k,-1) ;
            if(y != -1) {
                if(x <= y) {
                    blal.put((y-x),blal.getOrDefault((y-x),0)+1) ;
//                    blal[y - x] += 1 ;
                }
                else {
//                    blal[(n-x)+y] += 1 ;
                    blal.put(((n-x)+y),blal.getOrDefault(((n-x)+y),0)+1) ;
                }
            }
    }
        );
        ArrayList<Integer> v = new ArrayList<>(blal.values());
        Collections.sort(v,(x,y)-> y-x);
        pr.println(v.get(0));
        /*--------------------------*/
        pr.close();
    }

    static PrintWriter pr = new PrintWriter(System.out);
    static class Reader {
        BufferedReader br;
        StringTokenizer st;
        public Reader() {
            br = new BufferedReader(new
                    InputStreamReader(System.in));
        }
        public Reader(String k) throws Exception {
            br = new BufferedReader(new FileReader(new File("input")));
        }
        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
            return Integer.parseInt(next());
        }
        long nextLong() {
            return Long.parseLong(next());
        }
        double nextDouble() {
            return Double.parseDouble(next());
        }
        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}