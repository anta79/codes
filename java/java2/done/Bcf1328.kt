import java.io.*
import java.util.*

fun main() {
    var printwriter = PrintWriter(System.out)
    var bufferedreader = BufferedReader(InputStreamReader(System.`in`))
    var stringtokenizer = StringTokenizer("")
    fun next(): String {
        while (!stringtokenizer.hasMoreElements()) {
            try {
                stringtokenizer = StringTokenizer(bufferedreader.readLine())
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return stringtokenizer.nextToken()
    }

    fun nextInt(): Int {
        return next().toInt()
    }

    fun nextLong(): Long {
        return next().toLong()
    }

    fun nextDouble(): Double {
        return next().toDouble()
    }

    fun nextLine(): String {
        var str: String = ""
        try {
            str = bufferedreader.readLine()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return str
    }

    fun write(any: Any) {
        printwriter.print(any)
    }

    fun input() {
        bufferedreader = BufferedReader(FileReader(File("input")))
    }

    fun close() {
        printwriter.close()
    }


//    input()
    /*------------------*/
    var tc = nextInt()
    repeat(tc) {
        var n = nextInt()
        var k = nextInt()
        var sum = 0
        var p = 0
        for (i in 1..100000) {
            sum += i
            if (sum >= k) {
                p = i
                break
            }
        }
        var d = (p - 1) - (sum - k)
        var arr = Array(n, { 'a' })
        arr[p] = 'b'
        arr[d] = 'b'
        for(i in n-1 downTo 0) {
            write(arr[i])
        }
        write("\n")
    }
    /*------------------*/
    close()

}