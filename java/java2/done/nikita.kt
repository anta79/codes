import kotlin.math.max

class tup(x:Int, y:Int) {
    var a = x
    var b = y
}

fun solve(s : String)  {
    var arr = ArrayList<tup>()
    var l = s.length
    var t = false
    var x = 0
    var y = 0
    for( i in 0..l-1) {
        if(!t && s[i] == 'b') {
            x = i
            t = true
        }
        if(t && s[i] != 'b') {
            y = i
            arr.add(tup(x,y-1))
            t = false
        }
    }
    if(s[l-1] == 'b') {
        arr.add(tup(x,l-1))
    }
    var pre = Array(l,{0})
    if(s[0] == 'a') {
        pre[0] = 1
    }
    for (i in 1..l-1) {
        pre[i] = pre[i-1]
        if(s[i] == 'a') {
            pre[i] += 1
        }
    }
    var mx = 0
    arr.forEach { i->
        var ans = (i.b - i.a) + 1 + pre[i.a] + (pre[l-1]-pre[i.b])
        mx = max(mx,ans)
    }
    print(mx)
}

fun main() {
    solve("abab")
}