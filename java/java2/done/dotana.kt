fun main() {
    var k : Int
    var m : Int = 3
    k = 2
    
    if(m > k) {
        m = k.also { k = m }
    }

    println("$k $m")
}