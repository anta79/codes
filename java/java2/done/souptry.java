import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;


public class souptry {
    public static void main(String[] args) throws IOException {


        Document doc = Jsoup.connect("http://en.wikipedia.org/").get();

        Elements newsHeadlines = doc.select("#mp-itn b a");
        System.out.println(newsHeadlines.size());
        for (Element headline: newsHeadlines) {
            System.out.printf("%s\n\t%s",headline.attr("title"),headline.absUrl("href"));
        }

    }
}
