import java.io.*
import java.util.*
import kotlin.math.hypot

fun main() {
    var printwriter = PrintWriter(System.out)
    var bufferedreader = BufferedReader(InputStreamReader(System.`in`))
    var stringtokenizer = StringTokenizer("")
    fun next(): String {
        while (!stringtokenizer.hasMoreElements()) {
            try {
                stringtokenizer = StringTokenizer(bufferedreader.readLine())
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return stringtokenizer.nextToken()
    }

    fun nextInt(): Int {
        return next().toInt()
    }

    fun nextLong(): Long {
        return next().toLong()
    }

    fun nextDouble(): Double {
        return next().toDouble()
    }

    fun nextLine(): String {
        var str: String = ""
        try {
            str = bufferedreader.readLine()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return str
    }

    fun write(any: Any) {
        printwriter.print(any)
    }

    fun input() {
        bufferedreader = BufferedReader(FileReader(File("input")))
    }

    fun close() {
        printwriter.close()
    }

    fun cal(d : Double,xarr:Array<Long>,varr:Array<Long>) : Double {
        var time = 0.0
        var s = xarr.size
        for(i in 0 until s) {
            time = Math.max(time,(Math.abs(d-xarr[i])*1.0) / varr[i])
        }
        return  time
    }

//    var p = Math.pow(3,4)
//    var d =  hypot(3,5)
//    print(p)

    close()

}