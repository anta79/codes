import java.io.*
import java.lang.StringBuilder
import java.util.*
import kotlin.collections.ArrayList

fun main() {
    var printwriter = PrintWriter(System.out)
    var bufferedreader = BufferedReader(InputStreamReader(System.`in`))
    var stringtokenizer = StringTokenizer("")
    fun next(): String {
        while (!stringtokenizer.hasMoreElements()) {
            try {
                stringtokenizer = StringTokenizer(bufferedreader.readLine())
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return stringtokenizer.nextToken()
    }

    fun nextInt(): Int {
        return next().toInt()
    }

    fun nextLong(): Long {
        return next().toLong()
    }

    fun nextDouble(): Double {
        return next().toDouble()
    }

    fun nextLine(): String {
        var str: String = ""
        try {
            str = bufferedreader.readLine()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return str
    }

    fun write(any: Any) {
        printwriter.print(any)
    }

    fun input() {
        bufferedreader = BufferedReader(FileReader(File("input")))
    }

    fun close() {
        printwriter.close()
    }

    input()

    var tc = nextInt()
    repeat(tc) {

        var n = nextInt()
        var str = nextLine()

        var en1 = false

        var arr = Array(n,{'1'})
        var brr = Array(n,{'1'})

        for( i in 1..n-1) {
            if(str[i] == '0') {
                arr[i] = '0'
                brr[i] = '0'
            }
            else if(str[i] == '1') {
                if(en1) {
                    arr[i] = '1'
                    brr[i] = '0'
                }
                else {
                    arr[i] = '0'
                    brr[i] = '1'
                }
                en1 = true
            }
            else {
                if(en1) {
                    arr[i] = '2'
                    brr[i] = '0'
                }
                else {
                    arr[i] = '1'
                    brr[i] = '1'
                }
            }
        }
        arr.forEach { i ->
            write(i)
        }
        write("\n")
        brr.forEach {  i ->
            write(i)
        }
        write("\n")
    }

    close()

}