fun main() {
    outer@ for(i in 4..100) {
        for(j in 2 until i) {
            if(i % j == 0) {
                continue@outer
            }
        }
        println("$i is prime")
    }
}