import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.StringBuilder
import java.util.*

fun main() {
    var sc = BufferedReader(InputStreamReader(System.`in`))
    var s =  sc.readLine()

    var pre = Array(s.length+1,{0})

    for(i in 1..s.length-1) {
        pre[i] = pre[i-1]
        if(s[i-1] == s[i]) {
            pre[i] += 1
        }
    }
    var ans = StringBuilder("")
    var q = sc.readLine().toInt()
    for(i in 1..q) {
        var line = sc.readLine().split(" ")
        var a = line[0].toInt()
        var b = line[1].toInt()
        ans.append(String.format("%d\n",pre[b-1]-pre[a-1]))
    }
    print(ans)
}