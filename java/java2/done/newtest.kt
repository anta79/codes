fun solve(io: ProblemIO) {
    var d = io.readInt()
    io.println(d)
}

fun main(args: Array<String>) {
    Thread({ val io = ProblemIO.files(); solve(io); io.close() }).start()
}