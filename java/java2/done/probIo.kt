import java.io.*
import java.util.*

class ProblemIO(val rd: BufferedReader, val wr: PrintWriter) {
    companion object {
        fun console() = ProblemIO(BufferedReader(InputStreamReader(System.`in`)), PrintWriter(System.out))
        fun files() =
                ProblemIO(BufferedReader(FileReader(File("input"))), PrintWriter(System.out))
    }

    var tok = StringTokenizer("")
    fun close() {
        rd.close(); wr.close()
    }

    fun <T> print(v: T) = wr.print(v)
    fun <T> println(v: T) = wr.println(v)
    fun readToken(): String {
        while (!tok.hasMoreTokens()) tok = StringTokenizer(rd.readLine())
        return tok.nextToken()
    }

    fun readLine(): String {
        tok = StringTokenizer("")
        return rd.readLine()
    }

    fun readInt(): Int = readToken().toInt()
    fun readLong(): Long = readToken().toLong()
    fun readDouble(): Double = readToken().toDouble()
}