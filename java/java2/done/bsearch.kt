import java.util.*
import kotlin.collections.HashSet
import kotlin.math.max
import kotlin.math.min

fun main() {
    var sc = Scanner(System.`in`)
    var t = Integer.valueOf(sc.nextLine())
    for (i in 1..t) {
        var x = 0
        var y = 0
        var s = sc.nextLine()
        var b = HashMap<Pair<Int,Int>,Int>()
        var cobra = HashSet<Pair<Int,Int>>()
        var temp = 0
        b[Pair(0,0)] = temp
        s.forEach { i ->
            var cur = Pair(x,y)

            if(i == 'W') {
                x -= 1
            }
            else if(i == 'E') {
                x += 1
            }
            else if(i == 'S') {
                y -= 1
            }
            else {
                y += 1
            }
            var next = Pair(x,y)
            if(b[next] == null) {
                temp += 1
                b[next] = temp
            }
            var r = b.getOrDefault(cur,0)
            var z= b.getOrDefault(next,0)
            cobra.add(Pair(min(r,z),max(r,z)))
        }
        var ans = cobra.size
        var t = s.length - ans
        println(ans*5+t)
    }
}