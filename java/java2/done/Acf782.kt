import java.io.*
import java.util.*
import kotlin.math.max

fun main() {
    var printwriter = PrintWriter(System.out)
    var bufferedreader = BufferedReader(InputStreamReader(System.`in`))
    var stringtokenizer = StringTokenizer("")
    fun next(): String {
        while (!stringtokenizer.hasMoreElements()) {
            try {
                stringtokenizer = StringTokenizer(bufferedreader.readLine())
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return stringtokenizer.nextToken()
    }

    fun nextInt(): Int {
        return next().toInt()
    }

    fun nextLong(): Long {
        return next().toLong()
    }

    fun nextDouble(): Double {
        return next().toDouble()
    }

    fun nextLine(): String {
        var str: String = ""
        try {
            str = bufferedreader.readLine()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return str
    }

    fun write(any: Any) {
        printwriter.print(any)
    }

    fun input() {
        bufferedreader = BufferedReader(FileReader(File("input")))
    }

    fun close() {
        printwriter.close()
    }



//    input()
    /*------------------*/

    var n = nextInt()
    var arr = Array(n+1,{false})

    var m = 0
    var cur = 0

    repeat(2*n) {
        var g = nextInt()
        if(!arr[g]) {
            cur += 1
            arr[g] = true
        }
        else {
            cur -= 1
        }
        m = max(m,cur)
    }
    write(m)

    /*------------------*/
    close()

}