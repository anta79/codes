import java.io.BufferedReader
import java.io.InputStreamReader

fun main() {
    var sc = BufferedReader(InputStreamReader(System.`in`))
    var d = sc.readLine().toInt()
    var arr_ = sc.readLine().split(" ")
    var arr = Array(d,{i -> arr_[i].toInt()})
    arr.forEach { i->
        println(i+1)
    }
}