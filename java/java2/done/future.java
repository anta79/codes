import java.util.Scanner;

public class future {
    // 0 null 2 false 1 true
    public static int[][][] dp ;
    public static int last ;
    public static int[][] requires ;
    public static int canit(int n,int sticks,int i) {
        int ans = 2;
        if(requires[n][i] > sticks || requires[n][i] == -1) {
            dp[n][sticks][i] = 2 ;
            return 2 ;
        }
        int remain = sticks - requires[n][i] ;
        if(n == last) {
            if(remain == 0) {
                dp[n][sticks][i] = 1 ;
                return 1 ;
            }
            dp[n][sticks][i] = 2 ;
            return 2 ;
        }
        for (int j=9;j>-1;j--) {
            if(dp[n+1][remain][j] == 0) {
                ans = Math.min(ans,canit(n+1,remain,j));
            }
            else {
                ans = Math.min(ans, dp[n + 1][remain][j]);
            }
            if(ans == 1){
                break;
            }
        }
        dp[n][sticks][i] = ans ;
        return  ans ;
    }


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n,k ;
        n = sc.nextInt();
        k = sc.nextInt();
        String[] given = new String[n];
        for (int i=0;i<n;i++) {
            given[i] = sc.next();
        }

        String[] standards = {"1110111", "0010010", "1011101", "1011011", "0111010", "1101011", "1101111", "1010010", "1111111", "1111011"} ;
        requires = new int[n][10] ;
        for(int i=0;i<n;i++) {
            for(int j=0;j<10;j++) {
                for(int z=0;z<7;z++) {
                    if(standards[j].charAt(z) == '1' && given[i].charAt(z) =='0') {
                        requires[i][j] += 1 ;
                    }
                    if(standards[j].charAt(z) == '0' && given[i].charAt(z) =='1') {
                        requires[i][j] = -1 ;
                        break;
                    }
                }
            }
        }
        dp = new int[n+1][k+1][10] ;
        last = n-1 ;
        int cur = k ;
//        for(int i=0;i<n;i++) {
//            boolean ok = false ;
//            for(int j=9;j>-1;j--) {
//                if(canit(i,cur,j) == 1) {
//                    System.out.print(j);
//                    cur -= requires[i][j] ;
//                    ok = true ;
//                    break;
//                }
//            }
//            if(!ok) {
//                System.out.print(-1);
//                System.exit(0);
//            }
//        }
        System.out.println(canit(0,k,9));
    }
}
